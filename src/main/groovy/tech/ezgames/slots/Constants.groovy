package tech.ezgames.slots

/**
 * Created by idan on 08 Nov 2016.
 */
final class Constants {

    private Constants() { }

    static final def STORE_TO_SKU_TO_PRODUCT_ID =
            [
                    "play.google.com" : [
                            "s_200_usd_pack" : "200_usd_pack",
                            "s_100_usd_pack" : "100_usd_pack",
                            "s_50_usd_pack": "50_usd_pack",
                            "s_20_usd_pack": "20_usd_pack",
                            "s_30_usd_pack": "30_usd_pack",
                            "s_15_usd_pack": "15_usd_pack",
                            "s_10_usd_pack": "10_usd_pack",
                            "s_8_usd_pack": "8_usd_pack",
                            "s_5_usd_pack": "5_usd_pack_new",
                            "s_2_usd_pack": "2_usd_pack",
                            "o_SpecialOffer": "5_usd_pack_new",
                            "o_OutOfMoneyOffer": "4_usd_pack",
                            "p_break1" : "2_usd_pack",
                            "p_break2" : "5_usd_pack_new",
                            "p_break3": "10_usd_pack",
                            "p_break4": "15_usd_pack",
                            "p_break5": "30_usd_pack"
                    ].asImmutable(),
                    "itunes.apple.com" : [
                            "s_200_usd_pack" : "200_usd_pack.itunes",
                            "s_100_usd_pack" : "100_usd_pack.itunes",
                            "s_50_usd_pack": "50_usd_pack.itunes",
                            "s_20_usd_pack": "20_usd_pack.itunes",
                            "s_30_usd_pack": "30_usd_pack.itunes",
                            "s_15_usd_pack": "15_usd_pack.itunes",
                            "s_10_usd_pack": "10_usd_pack.itunes",
                            "s_8_usd_pack": "8_usd_pack.itunes",
                            "s_5_usd_pack": "5_usd_pack.itunes",
                            "s_2_usd_pack": "2_usd_pack.itunes",
                            "o_SpecialOffer": "5_usd_pack.itunes",
                            "o_OutOfMoneyOffer": "4_usd_pack.itunes",
                            "p_break1" : "2_usd_pack.itunes",
                            "p_break2" : "5_usd_pack.itunes",
                            "p_break3": "10_usd_pack.itunes",
                            "p_break4": "15_usd_pack.itunes",
                            "p_break5": "30_usd_pack.itunes"
                    ].asImmutable()
            ].asImmutable()

    static final def PERIODIC_BONUS_FREQUENCY_IN_MILLIS = 10800000

    static final def DAILY_MISSIONS_BONUS_FREQUENCY_IN_MILLIS = 86400000
}
