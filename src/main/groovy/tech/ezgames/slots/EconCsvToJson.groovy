package tech.ezgames.slots

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
/**
 * Created by idan on 08 Nov 2016.
 */
class EconCsvToJson {

    final List skus

    Table periodic = new Table(name: 'periodicBonus')
    Table dailyBonus = new Table(name: 'dailyMissionsBonus')
    Table connectToFacebook = new Table(name: 'facebookConnectBonus')
    Table levelup = new Table(name: 'levelUpBonus')
    Table turnover = new Table(name: 'turnover')

    Table betsTable = new Table(name: 'totalBets')

    Table piggyTaxTable = new Table(name: 'piggyTax')

    Table productsTable = new Table()

    Map<String, Table> dailyParamsTables = null


    EconCsvToJson() {
        Set set = [] as Set
        List list = []

        Constants.STORE_TO_SKU_TO_PRODUCT_ID.each { store, Map map  ->
            map.keySet().each {
                if (set.add(it))
                    list << it
            }
        }

        this.skus = list

        println "skus = $skus"
    }

    def initDailyParamsTables(CSVParser csv) {
        csv.headerMap.each {name, idx ->
            //first find the dailyMissionsParams marker column
            if (name == 'dailyMissionsParams') {
                this.dailyParamsTables = [:]
            }
            //each one after that is a mission
            else if (this.dailyParamsTables != null) {
                this.dailyParamsTables[name] = new Table(name: name)
            }
        }
    }

    def addDailyMissionsParamsRecords(CSVRecord csv) {
        int level = csv.level as int
        this.dailyParamsTables.each {name, table ->
            String value = csv.get(name)
            if (value) {
                addRecordIfChanged(table, level, value as long)
            }
        }
    }

    def parseFile(Path file) {
        file.withReader {reader ->
            CSVParser csv = new CSVParser(reader, CSVFormat.DEFAULT.withHeader())

            int bet1idx = csv.headerMap['Bet 1']
            initDailyParamsTables(csv)

            List storeItemNames = getStoreItemNames(csv)

            if (!skus.containsAll(storeItemNames)) {
                System.err.println "CSV contains unknown SKU: ${storeItemNames - skus}"
                System.exit (1)
            }

            csv.each {
                if (!it.level) {
                    return
                }
                int level = it.level as int
                println "Processing level $level"

                addRecordIfChanged(periodic, level, it.periodicBonus as long)
                addRecordIfChanged(dailyBonus, level, it.dailyMissionsBonus as long)
                addRecordIfChanged(connectToFacebook, level, it.facebookConnectBonus as long)
                addRecordIfChanged(turnover, level, it.turnover as long)
                addRecordIfChanged(piggyTaxTable, level, it.piggyTax as double)
                if (level > 1)
                    addRecordIfChanged(levelup, level, it.levelUpBonus as long)


                Bets aBets = readBets(it, bet1idx)
                addRecordIfChanged(betsTable, level, aBets)

                def storeItems = storeItemNames.collectEntries {s -> [(s): it[s] as long] }
                addRecordIfChanged(productsTable, level, storeItems)

                addDailyMissionsParamsRecords(it)
            }
        }
    }


    private List getStoreItemNames(CSVParser csv) {
        int storeIdx = csv.headerMap['store']
        int dailyMissionsParamsIdx = csv.headerMap['dailyMissionsParams']

        List storeItemNames = csv.headerMap.findAll { name, idx -> idx > storeIdx && idx < dailyMissionsParamsIdx}.collect { name, idx -> name }
        return storeItemNames
    }

    Bets readBets(CSVRecord csvRecord, int bet1idx) {
        def defaultBetIndexString = csvRecord.defaultBetIndex
        int defaultBetIndex = defaultBetIndexString ? defaultBetIndexString as int : -1

        List available = []

        for (int i = bet1idx; ; ++i) {
            def result = csvRecord.get(i)
            if (!result) break
            long aBet = result as long
            if (aBet == 0L) break
            available << aBet
        }

        if (defaultBetIndex == -1)
            defaultBetIndex = available.size() - 3
        if (defaultBetIndex < 0)
            defaultBetIndex = 0
        return new Bets(defaultBetIndex: defaultBetIndex, available: available)


    }

    def writeJson(OutputStream outputStream) {
        def root = new JsonSlurper().parse(this.class.getResourceAsStream('default.json'))

        [connectToFacebook, levelup, turnover, piggyTaxTable, betsTable].each {
            root[it.name] = it.map
        }

        root[periodic.name] = periodic.map.collectEntries {lvl,v ->
            [(lvl) : [bonus: v, frequencyInMillis: Constants.PERIODIC_BONUS_FREQUENCY_IN_MILLIS]]
        }

        root[dailyBonus.name] = dailyBonus.map.collectEntries { lvl, v ->
            [(lvl) : [bonus: v, frequencyInMillis: Constants.DAILY_MISSIONS_BONUS_FREQUENCY_IN_MILLIS]]
        }

        root['inAppProductsForDynamo'] = Constants.STORE_TO_SKU_TO_PRODUCT_ID.collectEntries { store, lookup ->
            [(store) : productsTable.map.collectEntries {lvl, m ->
                [(lvl) : m.collect {sku,coinValue ->
                    [sku: sku, productId: lookup[sku], coinValue: coinValue]
                }

            ]}]
        }

        root['dailyMissionsParams'] = this.dailyParamsTables.collectEntries {name,table -> [(table.name):table.map]}

        root.id = "${root.id}-${DateTimeFormatter.BASIC_ISO_DATE.format(LocalDateTime.now(ZoneOffset.UTC))}"
        root.createdTime = System.currentTimeMillis()
        root.version = 0



        PrintStream ps = new PrintStream(outputStream)

        ps.print(JsonOutput.prettyPrint(JsonOutput.toJson(root)))
    }


    def addRecordIfChanged(Table table, int level, def value) {
        if (!value) {
            return
        }

        if (table.lastValue == value)
            return
        table.lastValue = value
        table.map[level] = value
    }


    static void main(String[] args) {
        Path inputFile = Paths.get(args[0])

        if (!Files.isReadable(inputFile)) {
            System.err.println("Can't read from $inputFile")
            System.exit(1)
        }

        Path outputFile = Paths.get(args[1])
        if (!Files.exists(outputFile)) {
            Files.createFile(outputFile)
        }
        if (!Files.isWritable(outputFile)) {
            System.err.println("Can't write to $outputFile")
            System.exit(1)
        }

        EconCsvToJson econCsvToJson = new EconCsvToJson()

        econCsvToJson.parseFile(inputFile)

        outputFile.withOutputStream (econCsvToJson.&writeJson)
    }

    @ToString
    static class Table {
        String name
        def lastValue = null
        Map map = [:]
    }

    @ToString
    @EqualsAndHashCode
    static class Bets {
        int defaultBetIndex
        List available
    }

}
